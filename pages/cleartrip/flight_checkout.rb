
module Pages
  module Cleartrip
    module Flight

      class FlightCheckout

        def initialize(driver)
          @driver = driver
        end

        def apply_filters(filter)
          #### the filter param can be improved further to apply various sorts of filter
          ## from airline selection to departure time etc
          ##### and last non stop flight for return journey coded accordingly
          Wait.wait_for_element(@driver,BOOK_TICKETS,true)
          if filter[:non_stop]
            if !@driver.find_element(FLIGHT_STOPS(number_of_stop=0)).selected?
              @driver.find_element(FLIGHT_STOPS(number_of_stop=0)).click
            end

          end
        end

        def select_cheapest_onwards_flight
          # since cleartrip already gives a sorted list on the page we can directly select the first flight
          ## if not we can get the price for all & sort it in ascending order and select the cheapest
          price = ""
          all_flights = @driver.find_elements(FLIGHT_LIST).first
          price = all_flights.attribute 'data-price'

          if !all_flights.find_element(css: 'input').selected?
            all_flights.find_element(css: 'input').click
          end
          return price
        end

        def select_last_non_stop_return_flight
          price = ""
          all_flights = @driver.find_elements(FLIGHT_LIST).last
          price = all_flights.attribute 'data-price'

          if !all_flights.find_element(css: 'input').selected?
            all_flights.find_element(css: 'input').click
          end
          @driver.execute_script("scroll(1000,0)")
          return price
        end

        def fill_traveller_details(value)
          # this method accepts hash of hash
          # Sample hash for it to work as expected
          # value = { "Adult 1" => {first_name: "Lorem", last_name: "Ipsum", title: "Mr"}}
          Wait.wait_for_element(@driver,ADULT_FIRST_NAME(number=1))
          value.each do |k,v|
            case k.downcase
            when /adult/
              num = k.split(' ').last
              v.each do |key,val|
                case key
                when :first_name
                  @driver.find_element(ADULT_FIRST_NAME(number=num)).send_keys(val)
                when :last_name
                  @driver.find_element(ADULT_LAST_NAME(number=num)).send_keys(val)
                when :title
                  Selenium::WebDriver::Support::Select.new(@driver.find_element(ADULT_TITLE(number=num))).select_by(:value,val)
                end
              end
            when /child/
              num = k.split(' ').last
              v.each do |key,val|
                case key
                when :first_name
                  @driver.find_element(CHILD_FIRST_NAME(number=num)).send_keys(val)
                when :last_name
                  @driver.find_element(CHILD_LAST_NAME(number=num)).send_keys(val)
                when :title
                  Selenium::WebDriver::Support::Select.new(@driver.find_element(CHILD_TITLE(number=num))).select_by(:value,val)
                when :day
                  Selenium::WebDriver::Support::Select.new(@driver.find_element(CHILD_DOB_DAY(number=num))).select_by(:text,val)
                when :month
                  Selenium::WebDriver::Support::Select.new(@driver.find_element(CHILD_DOB_MONTH(number=num))).select_by(:text,val)
                when :year
                  Selenium::WebDriver::Support::Select.new(@driver.find_element(CHILD_DOB_YEAR(number=num))).select_by(:value,val)
                end

              end

            when /infant/
              # TO Do can be implemented later , as the assignment doesnt require to handle a case for infants skipping this
            end
          end
        end

        def proceed_to_checkout(details)
          # this method accepts hash of hash
          # Sample hash for it to work as expected
          # details = {"email"=> "lorem@yopmail.com", "traveller1"=> { "Adult 1" => {first_name: "Lorem", last_name: "Ipsum", title: "Mr"}}, "mobile"=> "1234567"}
          Wait.wait_for_element(@driver,BOOK_TICKETS,true)
          Wait.waitForPageLoad(@driver)

          @driver.find_elements(BOOK_TICKETS).last.click


          Wait.waitForPageLoad(@driver)

          if @driver.find_element(PRICE_CHANGE).displayed?
            @driver.find_element(PRICE_CHANGE).click
          end

          if !@driver.find_element(TERMS_CONDITIONS_CHECKBOX).selected?
            @driver.find_element(TERMS_CONDITIONS_CHECKBOX).click
          end
          @driver.find_elements(CONTINUE_BOOKING_BUTTON).each do |web_element|
            web_element.click if web_element.displayed?
          end

          details.each do |key,value|
            case key.downcase
            when 'username','email'
              @driver.find_element(USERNAME).send_keys(value)
              @driver.find_elements(CONTINUE_BOOKING_BUTTON).each do |web_element|
                web_element.click if web_element.displayed?
              end
              Wait.wait_for_element(@driver,ADULT_FIRST_NAME(number=1))
            when /traveller/
              raise 'Traveller details should be a hash' if !value.is_a? Hash
              fill_traveller_details(value)
            when /mobile/
              @driver.find_element(MOBILE_NUMBER).send_keys(value)
            end

          end

          @driver.execute_script("jQuery('##{CONTINUE_TO_PAYMENT[:id]}').click();")
          sleep 10 ### had to harcode the sleep , it fails everytime if i remove the sleep wait_for_element doesnt seem to be working only in this scenario
          Wait.wait_for_element(@driver,MAKE_PAYMENT)
          return @driver.find_element(MAKE_PAYMENT).displayed? ## to validate that we succesfully reach the payment page
        end

        private
        #### all the locators required in the Page will be below this point
        BOOK_TICKETS = { css: ".booking.fRight"}
        FLIGHT_LIST = {xpath: "//div[contains(@class,'listItem')]"}
        PRICE_CHANGE = {id: "priceChangeUpBtn"}
        TERMS_CONDITIONS_CHECKBOX = {id: "insurance_confirm"}
        CONTINUE_BOOKING_BUTTON = {xpath: "//input[contains(@value,'Continue')]"}
        USERNAME = {id: "username"}
        MOBILE_NUMBER = {css: ".required.mb.span.span8.placeholder"}
        CONTINUE_TO_PAYMENT = {id: "travellerBtn"}
        MAKE_PAYMENT = {id: "paymentSubmit"}

        #### Below locators need variable substitution ##
        def FLIGHT_STOPS(stop)
          {id: "1_1_#{stop}"}
        end

        def ADULT_FIRST_NAME(num)
          {id: "AdultFname#{num}"}
        end

        def ADULT_LAST_NAME(num)
          {id: "AdultLname#{num}"}
        end

        def ADULT_TITLE(num)
          {id: "AdultTitle#{num}"}
        end

        def CHILD_FIRST_NAME(num)
          {id: "ChildFname#{num}"}
        end

        def CHILD_LAST_NAME(num)
          {id: "ChildLname#{num}"}
        end

        def CHILD_TITLE(num)
          {id: "ChildTitle#{num}"}
        end

        def CHILD_DOB_DAY(num)
          {id: "ChildDobDay#{num}"}
        end

        def CHILD_DOB_MONTH(num)
          {id: "ChildDobMonth#{num}"}
        end

        def CHILD_DOB_YEAR(num)
          {id: "ChildDobYear#{num}"}
        end

      end

    end
  end
end
