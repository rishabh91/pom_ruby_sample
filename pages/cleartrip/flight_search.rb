module Pages
  module Cleartrip
    module Flight

      class FlightSearch

        def initialize(driver)
          @driver = driver
        end

        def fill_details(data)
          data.each do |key,value|
            case key
            when :trip_type
              if !@driver.find_element(TRIP_TYPE(value)).selected?
                @driver.find_element(TRIP_TYPE(value)).click
              end
              Wait.wait_for_element(@driver,RETURN_DATE)
            when :from
              @driver.find_element(FLIGHT_FROM).send_keys(value)
            when :to
              @driver.find_element(FLIGHT_TO).send_keys(value)
            when :departure
              raise "Incorrect Value, Should be of DateTimeClass" if value.class != Date
              @driver.execute_script("jQuery('##{DEPART_DATE[:id]}').val('#{value.strftime('%a, %d %b, %Y')}');")

            when :return
              raise "Incorrect Value, Should be of DateTimeClass" if value.class != Date
              @driver.execute_script("jQuery('##{RETURN_DATE[:id]}').val('#{value.strftime('%a, %d %b, %Y')}');")
            when :adult
              raise "Value should be in range of 1-9" if (value.to_i >9 || value.to_i < 1)
              Wait.wait_for_element(@driver,TRAVELLER_TYPE(key))
              Selenium::WebDriver::Support::Select.new(@driver.find_element(TRAVELLER_TYPE(key))).select_by(:value,value)
            when :child
              raise "Value should be in range of 0-8" if (value.to_i > 8 || value.to_i < 0)
              Wait.wait_for_element(@driver,TRAVELLER_TYPE(key))
              Selenium::WebDriver::Support::Select.new(@driver.find_element(TRAVELLER_TYPE(key))).select_by(:value,value)
            when :infant
              raise "Value should be in range of 0-1" if (value.to_i >1 || value.to_i < 0)
              Wait.wait_for_element(@driver,TRAVELLER_TYPE(key))
              @driver.find_element(TRAVELLER_TYPE(key)).click
              Selenium::WebDriver::Support::Select.new(@driver.find_element(TRAVELLER_TYPE(key))).select_by(:value,value)
            end
          end
          @driver.find_element(SEARCH).click
        end

        private
        #### all the locators required in the Page will be below this point
        FLIGHT_SEARCH_FORM = { xpath: "//form[@id='SearchForm']/h1[contains(.,'Search flights')]" }
        FLIGHT_FROM = {id: "FromTag"}
        FLIGHT_TO = {id: "ToTag"}
        DEPART_DATE = {id: "DepartDate"}
        RETURN_DATE = {id: "ReturnDate"}
        SEARCH = {id: "SearchBtn"}

        # Below locators need variable substitution

        def TRIP_TYPE(param)
          id = param.downcase
          case id
          when /one/
            id = "OneWay"
          when /round/
            id = "RoundTrip"
          when /multi/
            id = "MultiCity"
          end
          return {id: id}
        end

        def TRAVELLER_TYPE(type)
          id = type
          case id
          when /adult/
            id = "Adults"
          when /child/
            id = "Childrens"
          when /infant/
            id = "Infants"
          end
          return {id: id}
        end

      end

    end
  end
end
