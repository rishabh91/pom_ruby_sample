module Pages
  module Amazon

    class ProductDetailsPage

      def initialize(driver)
        @driver = driver
      end

      def get_book_price()
        ## since by default the paperback is selected not handling that condition
        @driver.find_element(BOOK_PRICE).text
      end

      def proceed_to_checkout
        @driver.find_element(BUY_NOW_BUTTON).click
      end

      def presence_of_payment_gateway
        ## this is to verify that after filling username & password user is redirected successfully to payment page
        # this method will return a boolean
        Wait.wait_for_element(@driver,PAYMENT)
        return @driver.find_element(PAYMENT).displayed?
      end

      private
      #### Locators ##############
      BOOK_PRICE = {xpath: "//div[@id='buyNewSection']//span[@class='currencyINR']/ancestor::span"}
      BUY_NOW_BUTTON = {id: "buy-now-button"}
      PAYMENT = {xpath: "//h1[contains(.,'Select a payment method')]"}
    end

  end
end
