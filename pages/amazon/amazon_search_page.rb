module Pages
  module Amazon
    class AmazonSearch

      def initialize(driver)
        @driver = driver
      end

      def search_for_product(product_name)
        # this search method works for exact match of product name, can be further enhanced to support
        # partial names as well
        Wait.wait_for_element(@driver,SEARCHBOX)
        @driver.find_element(SEARCHBOX).send_keys(product_name,:enter)
        Wait.wait_for_element(@driver,PRODUCT_NAME(product_name))
        product_url = @driver.find_element(PRODUCT_NAME(product_name)).attribute "href" # for avoding the opening of a new tab
        @driver.get(product_url)
      end

      private
      ## all the locators used will be in private
      SEARCHBOX = {id: "twotabsearchtextbox"}

      def PRODUCT_NAME(name)
        {link: name}
      end
    end

  end

end
