module Pages
  module Amazon

    class Login

      def initialize(driver)
        @driver = driver
      end

      def login(username,password)
        @driver.find_element(USERNAME).send_keys(username)
        @driver.find_element(PASSWORD).send_keys(password)
        @driver.find_element(SIGNIN_BUTTON).click
      end


      private
      ## Locators #########
      USERNAME = {id: "ap_email"}
      PASSWORD = {id: "ap_password"}
      SIGNIN_BUTTON = {id: "signInSubmit"}
    end
  end
end
