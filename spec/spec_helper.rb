require 'selenium-webdriver'
require 'require_all'
require 'pry'
require 'date'
require 'allure-rspec'
require 'csv'
require 'rspec'
require 'rspec/expectations'
require 'yaml'
require 'faker'
require 'pathname'
require 'fileutils'
require 'write_xlsx'
require_rel "../lib/"
require_rel "../pages/"

RSpec.configure do |config|
  config.include AllureRSpec::Adaptor
end

AllureRSpec.configure do |config|
  config.output_dir = "reports/allure"
  config.clean_dir = true
  config.logging_level = Logger::WARN
end
