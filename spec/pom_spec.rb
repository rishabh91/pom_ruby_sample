require 'spec_helper'

$flight_costs = 0
$book_cost = 0
$cab_fares = 1000
$food_expenses = 3000
$buffer = 2500
describe 'Cleartrip Flight Booking', :feature => "Ruby POM Sample" do

  before(:all) do
    @driver = WebDriver.new().driver
    @customer_details = Pages::Cleartrip::Flight::FlightSearch.new(@driver)
    @booking = Pages::Cleartrip::Flight::FlightCheckout.new(@driver)
    @util = Utils.new
  end

  after(:all) do
    @driver.quit
  end

  it "Checks booking of Cheapest non stop return tickets from Bangalore to Delhi",  :story => "Verifying the successful redirection to payments gateway on Cleartrip" do
    @driver.get("https://www.cleartrip.com")

    airport_list = @util.parse_csv("airport_details.csv") # getting test data from a file
    travel_details = { trip_type: "roundtrip",
                       from: airport_list["Bangalore"],
                       to: airport_list["Delhi"],
                       departure: Date.today + 14 , # as the anniversary is two weeks from now
                       return: Date.today + 15,
                       child: '1',
                       adult: '1'}

    Wait.waitForPageLoad(@driver)
    @customer_details.fill_details(travel_details)
    ## initiating the flight filtering #########

    filter = {non_stop: true}
    @booking.apply_filters(filter)
    onwards_journey_price = @booking.select_cheapest_onwards_flight
    return_journey_price = @booking.select_last_non_stop_return_flight
    $flight_costs = onwards_journey_price.to_i + return_journey_price.to_i
    checkout_details =  {"email"=>  Faker::Internet.email,
                         "traveller1"=> { "Adult 1" => {first_name: "Karan", last_name: "Singh", title: "Mr"}},
                         "traveller2" => {"Child 1" => {first_name: "Neha", last_name: "Singh", title: "Miss", day: "6", month: "Dec", year: "2008"}},
                         "mobile"=> Faker::Number.number(10)
                         }

    bool_payment_gateway = @booking.proceed_to_checkout(checkout_details)
    expect(bool_payment_gateway).to be true
  end

end

describe 'Buying Book from Amazon', :feature => "Ruby POM Sample" do

  before(:all) do
    @driver = WebDriver.new().driver
    @login = Pages::Amazon::Login.new(@driver)
    @search = Pages::Amazon::AmazonSearch.new(@driver)
    @product_details = Pages::Amazon::ProductDetailsPage.new(@driver)
  end

  after(:all) do
    @driver.quit
  end

  it "Checks buying of the book 'A Brief History of Everyone Who Ever Lived' from Amazon.in", :story => "Verifying the successful redirection to payments gateway" do
    @driver.get("https://amazon.in")
    @search.search_for_product("A Brief History of Everyone Who Ever Lived")
    book_price = @product_details.get_book_price.strip.to_f
    $book_cost = book_price
    @product_details.proceed_to_checkout

    ### please provide a valid Amazon username password for this step to get processed

    @login.login("lorem_ipsum@lorem.com","loremipsum")
    bool_payment_gateway = @product_details.presence_of_payment_gateway
    expect(bool_payment_gateway).to be true
  end

end

describe 'Writing all the expense data into ExcelSheet & CSV', :feature => "Ruby POM Sample" do
  before(:all) do
    @util = Utils.new
  end

  it "Writes all the data to a CSV file", :story => "Expense details to CSV file" do

    if File.exist?("#{Pathname.pwd}/test_data/expense_report.csv")
      FileUtils.rm("#{Pathname.pwd}/test_data/expense_report.csv")
    end
    expense_data = { "Trip to Delhi" => $flight_costs, "Cab fares for the trip" => $cab_fares, "Gift for Sandhya" => $book_cost, "Food expenses in Delhi" => $food_expenses,
                     "Buffer" => $buffer, "Total" => $flight_costs + $cab_fares + $book_cost + $food_expenses + $buffer}
    @util.hash_to_csv([expense_data],"expense_report.csv")
    bool_expense_report = false
    if File.exist?("#{Pathname.pwd}/test_data/expense_report.csv")
      bool_expense_report = true
    end
    expect(bool_expense_report).to be true
  end

  it "Writes all the data to a ExcelSheet", :story => "Expense details to Excel file" do

    if File.exist?("#{Pathname.pwd}/test_data/expense_report.xlsx")
      FileUtils.rm("#{Pathname.pwd}/test_data/expense_report.xlsx")
    end
    expense_data = { "Trip to Delhi" => $flight_costs, "Cab fares for the trip" => $cab_fares, "Gift for Sandhya" => $book_cost, "Food expenses in Delhi" => $food_expenses,
                     "Buffer" => $buffer, "Total" => $flight_costs + $cab_fares + $book_cost + $food_expenses + $buffer}

    workbook = WriteXLSX.new("#{Pathname.pwd}/test_data/expense_report.xlsx")
    worksheet = workbook.add_worksheet
    col = row = 0
    expense_data.each do |k,v|
      worksheet.write(row, col, k)
      worksheet.write(row+1,col,v)
      col += 1
    end
    workbook.close

    bool_expense_report = false
    if File.exist?("#{Pathname.pwd}/test_data/expense_report.xlsx")
      bool_expense_report = true
    end
    expect(bool_expense_report).to be true

  end

end
