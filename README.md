# Description
The POM (page object model) used in the assignment is inspired from "http://elementalselenium.com/tips/7-use-a-page-object" written by Dave Haeffner who is also the author of some of the best resources for ruby based selenium automations.

## Major Components of the Project:

RSpec sits at the core of this framework which drives the tests.

## WebDriver Class:
The WebDriver class is a simple wrapper written on top of selenium-webdriver to make it easy to creater driver instances keeping in mind the dry principle (dont repeat yourself) of ruby programming. The WebDriver class is controlled by the config.yml file which contains details like the type of browser to be used for testing, the default folder for files to be downloaded during testing etc. The config.yml file can be used to further enhance the capabilities of WebDriver class depending upon the project needs.  

## Util Class:
Contains utility methods like parse_csv which makes it easier for reading data from external files which in turn can help towards achieving data driven tests.

## Pages:
The Page classes are the core of the POM(page object model) design pattern. It helps to breaks down the application into small components. This design pattern makes code maintainence easier as whenever there is change in the UI elements we just have to update the locators at a single place present inside the respective pages. 

The common methods written inside the pages are generic in nature and are data driven in nature i.e. it will work according to the data provided to them. I have used ruby hashmaps as a data source for the tests.

## Reports:

I have used two different types of reports for this assignment. The first one is the simple RSpec html reports which gets generated after every run inside the rspec_reports folder. I have also used allure-rspec which is a open source reporting library built around RSpec, the reports generateed through that are very descriptive in nature.
