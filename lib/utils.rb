class Utils

  def parse_csv(file_name)
    data = CSV.read("#{Pathname.pwd}/test_data/#{file_name}",{ encoding: "UTF-8", headers: true, header_converters: :symbol, converters: :all})
    data_by_column = data.by_col()
    return Hash[data_by_column[0].zip(data_by_column[1])]
  end

  def hash_to_csv(array_of_hash,filename)
    CSV.open("#{Pathname.pwd}/test_data/#{filename}", "wb") do |csv|
      csv << array_of_hash.first.keys
      array_of_hash.each do |hash|
        csv << hash.values
      end
    end
  end
end
