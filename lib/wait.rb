module Wait

  class << self

    def wait_for_element(driver,element,multi=nil)
      counter = 0
      if multi
        while !driver.find_elements(element).last.displayed?
          puts counter
          sleep 1
          if counter == 15
            break
          end
          counter += 1
        end

      else
        while !driver.find_element(element).displayed?
          puts counter
          sleep 1
          if counter == 15
            break
          end
          counter += 1
        end
      end
    end

    def waitForPageLoad(driver)
      begin
        count = 0
        until driver.execute_script("return jQuery.active == 0")
          if count == 180
            driver.navigate.refresh
            return
          end
          print '.'
          count += 1
        end
      rescue Net::ReadTimeout => e
        puts "********************** NET READ TIMEOUT **********************"
        driver.navigate.refresh
      end
    end

  end
end
