class WebDriver
  attr_reader :driver
  @driver = nil

  def initialize()
    @config = YAML.load_file("config.yml")
    start_browser()
  end

  def start_browser()
    browser = @config['browser']

    case browser

    when 'firefox'

      if @config.has_key? "prefs"
        prefs = @config['prefs']
      end

      options = Selenium::WebDriver::Firefox::Options.new
      if prefs['profile']
        if prefs['profile']['name']
          profile = Selenium::WebDriver::Firefox::Profile.from_name(prefs['profile']['name'])
          options.profile = profile
        end
      end
      if prefs['download']
        options.add_preference("browser.download.folderList", 2)
        options.add_preference("browser.download.dir", "#{Pathname.pwd}/#{prefs['download']['default_directory']}")
        options.add_preference("browser.download.manager.alertOnEXEOpen", false)
        options.add_preference("browser.helperApps.neverAsk.saveToDisk" , "application/msword,application/csv,text/csv,image/png ,image/jpeg, application/pdf, text/html,text/plain,application/octet-stream")
      end
      @driver = Selenium::WebDriver.for :firefox, options: options

    when 'chrome'
      if @config.has_key? "switches"
        switches = @config['switches']
      end

      if @config.has_key? "prefs"
        prefs = @config['prefs']
      end

      options = Selenium::WebDriver::Chrome::Options.new

      if prefs
        prefs.each do |key, value|
          options.add_preference(key, value)
        end
      end

      switches.map { |k| options.add_argument(k) }

      @driver = Selenium::WebDriver.for(:chrome, options: options)

    when 'safari'
      # TO DO
    when 'ie'
      # TO DO
    when 'edge'
      # TO DO
    end

    @driver.manage.window.maximize #  to run browser on full screen
    @driver.manage.timeouts.implicit_wait = @config['implicit_wait']
  end



end
